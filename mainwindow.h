#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qmqtt.h>
#include <QTimer>
#include <QVBoxLayout>

class ThermoMeter;

const QHostAddress MQTT_HOST = QHostAddress("192.168.1.1");
const quint16 MQTT_PORT = 1883;
const QString TEMP_TOPIC = "/Node01/Temp";
const QString HUMI_TOPIC = "/Node01/Humi";

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onConnected();
    void onSubscribed(const QString& topic);
    void onReceived(const QMQTT::Message& message);
    void updateCurrentDateTime();

private:
    Ui::MainWindow *ui;
    QMQTT::Client *client;
    qint64 unixtime;
    qint64 currenttime;
    QTimer *timerCurrentDateTime;

    // Termometr
    ThermoMeter *thermo;
    QWidget *widget;
};

#endif // MAINWINDOW_H
