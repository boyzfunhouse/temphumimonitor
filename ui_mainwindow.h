/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QLabel *label_2;
    QLabel *txtTemp;
    QLabel *txtHumi;
    QLabel *txtCurrentDateTime;
    QLabel *txtLastUpdate;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(800, 480);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(90, 20, 210, 161));
        QFont font;
        font.setPointSize(30);
        label->setFont(font);
        label->setPixmap(QPixmap(QString::fromUtf8(":/images/Temperature-104.png")));
        label->setAlignment(Qt::AlignCenter);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(510, 20, 210, 161));
        label_2->setFont(font);
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/images/Humidity-104.png")));
        label_2->setAlignment(Qt::AlignCenter);
        txtTemp = new QLabel(centralWidget);
        txtTemp->setObjectName(QStringLiteral("txtTemp"));
        txtTemp->setGeometry(QRect(10, 190, 370, 130));
        QFont font1;
        font1.setPointSize(80);
        txtTemp->setFont(font1);
        txtTemp->setAlignment(Qt::AlignCenter);
        txtHumi = new QLabel(centralWidget);
        txtHumi->setObjectName(QStringLiteral("txtHumi"));
        txtHumi->setGeometry(QRect(430, 190, 360, 130));
        txtHumi->setFont(font1);
        txtHumi->setAlignment(Qt::AlignCenter);
        txtCurrentDateTime = new QLabel(centralWidget);
        txtCurrentDateTime->setObjectName(QStringLiteral("txtCurrentDateTime"));
        txtCurrentDateTime->setGeometry(QRect(0, 420, 800, 30));
        txtLastUpdate = new QLabel(centralWidget);
        txtLastUpdate->setObjectName(QStringLiteral("txtLastUpdate"));
        txtLastUpdate->setGeometry(QRect(0, 390, 800, 30));
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        label->setText(QString());
        label_2->setText(QString());
        txtTemp->setText(QApplication::translate("MainWindow", "<html><head/><body><p>30\302\260C</p></body></html>", 0));
        txtHumi->setText(QApplication::translate("MainWindow", "<html><head/><body><p>60%</p></body></html>", 0));
        txtCurrentDateTime->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        txtLastUpdate->setText(QApplication::translate("MainWindow", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
