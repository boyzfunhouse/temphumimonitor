#-------------------------------------------------
#
# Project created by QtCreator 2016-03-28T00:22:41
#
#-------------------------------------------------

unix{
    POST_TARGETDEPS += ./analogwidgets/libanalogwidgets.a
    LIBS += ./analogwidgets/libanalogwidgets.a
}

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TempHumiMonitor
TEMPLATE = app
target.files = $${TARGET}
target.path = /opt/$${TARGET}/bin
INSTALLS = target

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

INCLUDEPATH += ../src
INCLUDEPATH += ./analogwidgets/analogwidgets \
               ./analogwidgets
LIBS += -L../src -lqmqtt

RESOURCES += \
    resource.qrc
