#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "thermometer.h"
#include <QDateTime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    client = new QMQTT::Client(MQTT_HOST, MQTT_PORT);
    client->setClientId("QT_MQTT_CLIENT");
    connect(client, &QMQTT::Client::connected, this, &MainWindow::onConnected);
    connect(client, &QMQTT::Client::subscribed, this, &MainWindow::onSubscribed);
    connect(client, &QMQTT::Client::received, this, &MainWindow::onReceived);
    client->connectToHost();

    ui->setupUi(this);
    unixtime = QDateTime::currentDateTime().toTime_t();
    currenttime = QDateTime::currentDateTime().toTime_t();

    timerCurrentDateTime = new QTimer(this);
    connect(timerCurrentDateTime, SIGNAL(timeout()), this, SLOT(updateCurrentDateTime()));
    timerCurrentDateTime->start(1000);

    widget = ui->centralWidget;
    thermo = new ThermoMeter();
    thermo->resize(10,100);
    QLayout *layout2 = new QVBoxLayout();
    layout2->addWidget(thermo);
    widget->setLayout(layout2);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::onConnected()
{
    qDebug() << "connected" << endl;
    client->subscribe(TEMP_TOPIC, 0);
    client->subscribe(HUMI_TOPIC, 0);
}

void MainWindow::onSubscribed(const QString& topic)
{
    qDebug() << "subscribed " << topic << endl;
}

void MainWindow::onReceived(const QMQTT::Message& message)
{
    qDebug() << "publish received: \"" << QString::fromUtf8(message.payload())
          << "\"" << endl;
    if (message.topic().contains("Temp"))
    {
        ui->txtTemp->setText(QString::fromUtf8(message.payload())+"°C");
        thermo->setValue((int)QString::fromUtf8(message.payload()).toFloat());
    } else if (message.topic().contains("Humi"))
    {
        ui->txtHumi->setText(QString::fromUtf8(message.payload())+"%");
    }
    unixtime = QDateTime::currentDateTime().toTime_t();
    ui->txtLastUpdate->setText("Last update : "+QDateTime::fromTime_t(unixtime).toString("dd/MM/yyyy HH:mm:ss"));
}

void MainWindow::updateCurrentDateTime()
{
    currenttime = QDateTime::currentDateTime().toTime_t();
    ui->txtCurrentDateTime->setText("Current datetime : "+QDateTime::fromTime_t(currenttime).toString("dd/MM/yyyy HH:mm:ss"));
}
